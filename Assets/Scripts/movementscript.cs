﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class movementscript : MonoBehaviour {

	public float ms;
	public GameObject light;
	public GameObject flare;
	public GameObject player;
	public GameObject parent;

	//doors for the opening
	public GameObject door1A;
	public GameObject door1B;
	public GameObject door1C;

	public GameObject door2A;
	public GameObject door2B;
	public GameObject door2C;

	public GameObject door3A;
	public GameObject door3B;
	public GameObject door3C;

	public GameObject door4A;
	public GameObject door4B;
	public GameObject door4C;

	public Vector3 reset;

	public Text endtext;

	public AudioSource door;
	public AudioSource heartbeat;
	public AudioSource soundtrack;



	private Rigidbody rb;
	public static float count;
	private Transform temp;

	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		heartbeat.Play ();
		soundtrack.Play ();
	}

	void FixedUpdate ()
	{
		/*
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		*/
		float movePitch = Input.GetAxis ("Pitch");
		float moveTurn = Input.GetAxis ("Turn");
		float movePitch2 = Input.GetAxis ("Pitch2");

		//Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		Vector3 rotation = new Vector3 (moveTurn, movePitch, movePitch2);
		//Vector3 rotation = new Vector3 (moveTurn, 0.0f, movePitch);

		//rb.AddForce (movement * speed);
		rb.transform.Rotate (rotation);

	}

	void OnTriggerEnter(Collider other) 
	{

	}

	void Update()
	{

		if (Input.GetKeyDown(KeyCode.R))
			Application.LoadLevel ("main");

		if (Input.GetKeyDown (KeyCode.F)) 
		{
			if(light.activeSelf == true)
			{
				light.SetActive(false);
			}
			else
			{
				light.SetActive(true);
			}
		}
			
		if (Input.GetKeyDown (KeyCode.H)) 
		{
			if (count < 6) {
				var thePosition = transform.TransformPoint(Vector3.down * 1.1f);
				Instantiate(flare, thePosition, rb.transform.rotation);
				count++;
			}
		}
			
		if (Input.GetKey (KeyCode.W)) {
			transform.position += transform.forward * Time.deltaTime * ms;
		} else if (Input.GetKey (KeyCode.S)) {
			rb.position -= transform.forward * Time.deltaTime * ms;
		} else if (Input.GetKey (KeyCode.A)) {
			rb.position -= transform.right * Time.deltaTime * ms;
		} else if (Input.GetKey (KeyCode.D)) {
			rb.position += transform.right * Time.deltaTime * ms;
		} else if (Input.GetKey (KeyCode.Z)) {
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;
		} else if (Input.GetKey (KeyCode.Space)) {
			if (doorscript.door1 == true) {
				door1A.GetComponent<Rigidbody> ().position -= transform.right * 5;
				door1B.GetComponent<Rigidbody> ().position += transform.right * 5;
				door1C.SetActive (false);
				door.Play ();
				doorscript.door1 = false;
			} else if (doorscript2.door2 == true) {
				door2A.GetComponent<Rigidbody> ().position -= transform.right * 5;
				door2B.GetComponent<Rigidbody> ().position += transform.right * 5;
				door2C.SetActive (false);
				doorscript2.door2 = false;
				door.Play ();
			} else if (doorscript3.door3 == true) {
				door3A.GetComponent<Rigidbody> ().position -= transform.right * 5;
				door3B.GetComponent<Rigidbody> ().position += transform.right * 5;
				door3C.SetActive (false);
				doorscript3.door3 = false;
				door.Play ();
			} else if (doorscript4.door4 == true) {
				door4A.GetComponent<Rigidbody> ().position -= transform.right * 5;
				door4B.GetComponent<Rigidbody> ().position += transform.right * 5;
				door4C.SetActive (false);
				doorscript4.door4 = false;
				door.Play ();
			}
		} 
		else if (endscript.endg == true) {
			endtext.text = "YOU WON!";
		}

			

	}
}
